import { SECTIONS } from "~/common/data";
import {
  ADD_ENTRY,
  DELETE_ENTRY,
  EDIT_ENTRY,
  EDIT_SECTION_NAME,
  SET_ENTRIES,
  SET_SECTIONS
} from "./mutations-constants";

export const state = () => ({
  sections: SECTIONS as Section[]
});

export const mutations = {
  [ADD_ENTRY]: (state: any, payload: any) => {
    let uniqueKey = payload.uniqueKey;
    let entry = payload.entry;
    let copySection = [...state.sections];
    let indexOfSection = copySection.findIndex(section => {
      return section.uniqueKey == uniqueKey;
    });
    copySection[indexOfSection].entries.push(entry);
    state.sections = copySection;
  },
  [EDIT_SECTION_NAME]: (state: any, payload: any) => {
    let value = payload.value;
    let identifier = payload.uniqueKey;
    let copySection = [...state.sections];
    let indexOfSection = copySection.findIndex(section => {
      return section.uniqueKey == identifier;
    });
    copySection[indexOfSection].name = value;
    state.sections = copySection;
  },
  [DELETE_ENTRY]: (state: any, payload: any) => {
    let sectionUniqueKey = payload.sectionUniqueKey;
    let entryUniqueKey = payload.entryUniqueKey;
    console.log(sectionUniqueKey);
    console.log(entryUniqueKey);
    let copySection = [...state.sections];
    let indexOfSection = copySection.findIndex(section => {
      return section.uniqueKey == sectionUniqueKey;
    });

    let filteredEntries = copySection[indexOfSection].entries.filter(
      (entry: Entry) => {
        return entry.id != entryUniqueKey;
      }
    );
    copySection[indexOfSection].entries = filteredEntries;
    state.sections = copySection;
  },
  [EDIT_ENTRY]: (state: any, payload: any) => {
    let sectionUniqueKey = payload.sectionUniqueKey;
    let entryUniqueKey = payload.entryUniqueKey;
    let fieldUniqueKey = payload.fieldUniqueKey;
    let value = payload.value;

    let copySection = [...state.sections];
    let indexOfSection = copySection.findIndex(section => {
      return section.uniqueKey == sectionUniqueKey;
    });
    let updatedEntries = copySection[indexOfSection].entries.map(
      (entry: Entry) => {
        if (entry.id === entryUniqueKey) {
          let fields = entry.fields.map((field: Field) => {
            if (field.uniqueKey === fieldUniqueKey) {
              field.content = value;
            }
            return field;
          });
          entry.fields = fields;
          return entry;
        }
        return entry;
      }
    );
    copySection[indexOfSection].entries = updatedEntries;
    state.sections = copySection;
  },
  [SET_ENTRIES]: (state: any, payload: any) => {
    let sectionKey = payload.sectionUniqueKey;
    let entries = payload.entries;
    state.sections.find((section: Section) => {
      return section.uniqueKey == sectionKey;
    }).entries = entries;
  },
  [SET_SECTIONS]: (state: any, sections: Section[]) => {
    state.sections = sections;
  }
};

export const getters = {
  sectionNames: (state: any) => {
    return state.sections.map((section: Section) => {
      return section.name;
    });
  },
  sectionByKey: (state: any) => (uniqueKey: string) => {
    return state.sections.find((section: Section) => {
      return section.uniqueKey == uniqueKey;
    });
  },
  sections: (state: any) => {
    return state.sections;
  }
};
