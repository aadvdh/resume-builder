import {
  SET_COLOR,
  SET_FONT_FAMILY,
  SET_FONT_SIZE,
  SET_SECTION_SPACING
} from "./mutations-constants";

export const state = () => ({
  fontSize: 85 as Number,
  sectionSpacing: 20 as Number,
  fontFamily: "Poppins" as string,
  color: "#2563EBFF" as string
});

export const mutations = {
  [SET_FONT_SIZE]: (state: any, size: number): void => {
    state.fontSize = size;
  },
  [SET_SECTION_SPACING]: (state: any, spacing: number): void => {
    state.sectionSpacing = spacing;
  },
  [SET_FONT_FAMILY]: (state: any, family: string): void => {
    state.fontFamily = family;
  },
  [SET_COLOR]: (state: any, color: string): void => {
    state.color = color;
  }
};

export const getters = {
  theme: (state: any) => {
    return {
      fontSize: state.fontSize,
      sectionSpacing: state.sectionSpacing,
      fontFamily: state.fontFamily,
      color: state.color
    };
  }
};

export const actions = {};
