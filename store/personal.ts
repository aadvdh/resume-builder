import { findIndexOfObjectByKeyValue, makeField } from "./../common/helpers";
import { PersonalFields } from "~/common/data";
import {
  ADD_FIELD,
  SET_FIELD_CONTENT,
  SET_PROFILE_PHOTO
} from "./mutations-constants";
import { v4 as uuidv4 } from "uuid";
export const state = () => ({
  personalFields: PersonalFields as Field[],
  profilePhoto: "" as string
});

export const mutations = {
  [SET_FIELD_CONTENT]: (state: any, payload: any) => {
    const copyState = [...state.personalFields];
    let index = findIndexOfObjectByKeyValue(
      copyState,
      "uniqueKey",
      payload.key
    );
    const newField = { ...copyState[index], content: payload.value };
    copyState[index] = newField;

    state.personalFields = copyState;
  },
  [ADD_FIELD]: (state: any, payload: any) => {
    const field = makeField(payload.type, "text", uuidv4());
    field.content = payload.content;
    state.personalFields = [...state.personalFields, field];
  },
  [SET_PROFILE_PHOTO]: (state: any, url: string) => {
    state.profilePhoto = url;
  }
};

export const getters = {
  fieldIndexByKey: (state: any) => (uniqueKey: string) => {
    let field = findIndexOfObjectByKeyValue(
      state.PersonalFields,
      "uniqueKey",
      uniqueKey
    );
    return field;
  },
  profilePhoto: (state: any) => {
    return state.profilePhoto;
  },
  fields: (state: any): Array<Field> => {
    return state.personalFields;
  },
  fieldsToObject: (state: any) => {
    return state.personalFields.reduce((map: any, obj: Field) => {
      map[obj.uniqueKey] = obj.content;
      return map;
    }, {});
  }
};
