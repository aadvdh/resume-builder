import { MENU_ITEMS } from "./../common/data";
import {
  REMOVE_ENTRIES_DRAGGABLE,
  REMOVE_SECTIONS_DRAGGABLE,
  SET_ACTIVE_MENU_ITEM,
  TOGGLE_CONTENT_EDITABLE,
  TOGGLE_ENTRIES_DRAGGABLE,
  TOGGLE_SECTIONS_DRAGGABLE
} from "./mutations-constants";

export const state = () => ({
  activeMenuItem: MENU_ITEMS[0].name as string,
  contentEditable: false,
  entriesDraggable: false,
  sectionsDraggable: false
});

export const getters = {
  isMenuItemActive: (state: any) => (menuItemName: string) => {
    return state.activeMenuItem === menuItemName;
  },
  activeMenuItem: (state: any) => {
    return state.activeMenuItem;
  },
  isContentEditable: (state: any) => {
    return state.contentEditable;
  },
  isEntryDraggable: (state: any) => {
    return state.entriesDraggable;
  },
  isSectionDraggable: (state: any) => {
    return state.sectionsDraggable;
  }
};

export const mutations = {
  [SET_ACTIVE_MENU_ITEM]: (state: any, menuItemName: string) => {
    state.activeMenuItem = menuItemName;
  },
  [TOGGLE_CONTENT_EDITABLE]: (state: any) => {
    state.contentEditable = !state.contentEditable;
  },
  [TOGGLE_ENTRIES_DRAGGABLE]: (state: any) => {
    state.entriesDraggable = !state.entriesDraggable;
  },
  [TOGGLE_SECTIONS_DRAGGABLE]: (state: any) => {
    state.sectionsDraggable = !state.sectionsDraggable;
  },
  [REMOVE_ENTRIES_DRAGGABLE]: (state: any) => {
    state.sectionsDraggable = false;
  },
  [REMOVE_SECTIONS_DRAGGABLE]: (state: any) => {
    state.entriesDraggable = false;
  }
};

export const actions = {};
