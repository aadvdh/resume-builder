module.exports = {
  purge: [],
  mode: "jit",
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      transitionProperty: {
        height: "height"
      }
    }
  },
  variants: {
    extend: {
      backgroundColor: ["active"],
      textColor: ["active"],
      borderRadius: ["first", "last"]
    }
  },
  plugins: []
};
