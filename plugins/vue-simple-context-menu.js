import Vue from "vue";
import "vue-simple-context-menu/dist/vue-simple-context-menu.css";

import VueSimpleContextMenu from "vue-simple-context-menu";

Vue.use("vue-simple-context-menu", VueSimpleContextMenu);
