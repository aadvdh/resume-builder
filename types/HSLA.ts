interface HSLA {
  hue: Number;
  saturation: Number;
  luminosity: Number;
  alpha: Number;
}
