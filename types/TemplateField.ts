interface TemplateField {
  type: "text" | "range" | "textarea";
  uniqueKey: string;
  label: string;
}
