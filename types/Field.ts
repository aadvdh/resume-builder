interface Field {
  type: "text" | "range" | "textarea" | "email" | "url";
  uniqueKey: string;
  label: string;
  content: string;
  full?: boolean;
}
