interface Section {
  name: string;
  uniqueKey: string;
  entries: Array<Entry>;
  template: Array<Field>;
}
