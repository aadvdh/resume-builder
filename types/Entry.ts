interface Entry {
  id: string;
  fields: Array<Field>;
}
