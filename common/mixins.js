import store from "~/store";
import { mapGetters } from "vuex";
import {
  DELETE_ENTRY,
  EDIT_ENTRY,
  EDIT_SECTION_NAME,
  SET_ENTRIES,
  SET_SECTIONS
} from "~/store/mutations-constants";
export const templateMixin = {
  computed: {
    ...mapGetters([
      "isContentEditable",
      "isEntryDraggable",
      "isSectionDraggable"
    ]),
    ...mapGetters("theme", ["theme"]),
    ...mapGetters("sections", ["sections", "sectionByKey"]),
    ...mapGetters("personal", ["fields", "fieldsToObject", "profilePhoto"])
  },
  methods: {
    updateSectionName(uniqueKey, value) {
      this.$store.commit(`sections/${EDIT_SECTION_NAME}`, { uniqueKey, value });
    },
    updateEntry(sectionUniqueKey, entryUniqueKey, fieldUniqueKey, value) {
      this.$store.commit(`sections/${EDIT_ENTRY}`, {
        sectionUniqueKey,
        entryUniqueKey,
        fieldUniqueKey,
        value
      });
    },
    setEntries(sectionUniqueKey, entries) {
      this.$store.commit(`sections/${SET_ENTRIES}`, {
        sectionUniqueKey,
        entries
      });
    },
    setSections(sections) {
      this.$store.commit(`sections/${SET_SECTIONS}`, sections);
    },
    deleteEntry(sectionUniqueKey, entryUniqueKey) {
      this.$store.commit(`sections/${DELETE_ENTRY}`, {
        sectionUniqueKey,
        entryUniqueKey
      });
    }
  }
};
