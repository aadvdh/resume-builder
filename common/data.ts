export const EDUCATION = "Education";
export const PROFILE = "Profile";
export const EXPERIENCE = "Experience";
export const PROJECTS = "Projects";
export const AWARDS = "Awards";
export const VOLUNTEERING = "Volunteering";
export const SKILLS = "Skills";
export const LANGUAGES = "Languages";
import { v4 as uuidv4 } from "uuid";
export const EducationFields: Array<Field> = [
  {
    uniqueKey: "degree",
    type: "text",
    label: "Degree",
    content: "",
    full: false
  },
  {
    uniqueKey: "areaOfStudy",
    type: "text",
    label: "Area of Study",
    content: "",
    full: false
  },
  {
    uniqueKey: "school",
    type: "text",
    label: "School or University",
    content: "",
    full: false
  },
  {
    uniqueKey: "date",
    type: "text",
    label: "Dates Attended",
    content: "",
    full: false
  },
  {
    uniqueKey: "description",
    type: "textarea",
    label: "Description, Honors, etc.",
    content: "",
    full: true
  }
];
export const ExperienceFields: Array<Field> = [
  {
    uniqueKey: "title",
    type: "text",
    label: "Job Title",
    content: "",
    full: false
  },
  {
    uniqueKey: "company",
    type: "text",
    label: "Employer or Company",
    content: "",
    full: false
  },
  {
    uniqueKey: "date",
    type: "text",
    label: "Employment Dates",
    content: "",
    full: false
  },
  {
    uniqueKey: "location",
    type: "text",
    label: "Company Location",
    content: "",
    full: false
  },
  {
    uniqueKey: "description",
    type: "textarea",
    label: "Job Description & Accomplishments",
    content: "",
    full: true
  }
];

export const ProfileFields: Array<Field> = [
  {
    uniqueKey: "profile",
    type: "textarea",
    label: "Your Profile",
    content: "",
    full: true
  }
];

export const ProjectFields: Array<Field> = [
  {
    uniqueKey: "title",
    type: "text",
    label: "Project Title",
    content: "",
    full: false
  },
  {
    uniqueKey: "type",
    type: "text",
    label: "Type Of Project",
    content: "",
    full: false
  },
  {
    uniqueKey: "date",
    type: "text",
    label: "Project Date",
    content: "",
    full: false
  },
  {
    uniqueKey: "reason",
    type: "text",
    label: "Reason Or Purpose",
    content: "",
    full: false
  },
  {
    uniqueKey: "description",
    type: "textarea",
    label: "Project Description",
    content: "",
    full: true
  }
];

export const AwardFields: Array<Field> = [
  {
    uniqueKey: "title",
    type: "text",
    label: "Award Title",
    content: "",
    full: false
  },
  {
    uniqueKey: "from",
    type: "text",
    label: "Received From",
    content: "",
    full: false
  },
  {
    uniqueKey: "date",
    type: "text",
    label: "Date or Year Received",
    content: "",
    full: false
  },
  {
    uniqueKey: "link",
    type: "text",
    label: "Link",
    content: "",
    full: false
  },
  {
    uniqueKey: "description",
    type: "textarea",
    label: "Award Description",
    content: "",
    full: true
  }
];

export const VolunteeringFields: Array<Field> = [
  {
    uniqueKey: "title",
    type: "text",
    label: "Title",
    content: "",
    full: false
  },
  {
    uniqueKey: "date",
    type: "text",
    label: "Dates Volunteering",
    content: "",
    full: false
  },
  {
    uniqueKey: "reason",
    type: "text",
    label: "Organization Or Reason",
    content: "",
    full: true
  },

  {
    uniqueKey: "description",
    type: "textarea",
    label: "Description",
    content: "",
    full: true
  }
];
export const LanguageFields: Array<Field> = [
  {
    uniqueKey: "language",
    type: "text",
    label: "Language",
    content: "",
    full: false
  },
  {
    uniqueKey: "level",
    type: "text",
    label: "Skill Level",
    content: "",
    full: false
  }
];

export const PersonalFields: Array<Field> = [
  {
    uniqueKey: "name",
    type: "text",
    label: "Name",
    content: ""
  },
  {
    uniqueKey: "headlineOrTitle",
    type: "text",
    label: "Headline or Title",
    content: ""
  },
  {
    uniqueKey: "emailaddress",
    type: "email",
    label: "Email Address",
    content: ""
  },
  {
    uniqueKey: "phonenumber",
    type: "text",
    label: "Phone Number",
    content: ""
  },
  {
    uniqueKey: "linkedin",
    type: "text",
    label: "Linkedin",
    content: ""
  },
  {
    uniqueKey: "website",
    type: "url",
    label: "Website",
    content: ""
  },
  {
    uniqueKey: "",
    type: "text",
    label: "Address",
    content: ""
  }
];

export const MENU_ITEMS: Array<MenuItem> = [
  {
    name: "Personal",
    icon: "far fa-user",
    component: "MenuPersonal"
  },
  {
    name: "Content",
    icon: "far fa-pen",
    component: "MenuContent"
  },
  {
    name: "Theme",
    icon: " fa-tint",
    component: "MenuTheme"
  },
  {
    name: "Save",
    icon: " fas fa-save",
    component: "MenuSave"
  },
  {
    name: "SourceCode",
    icon: "fas fa-code",
    component: "MenuSourceCode"
  }
];

export const SECTIONS: Array<Section> = [
  {
    name: EDUCATION,
    entries: [],
    template: EducationFields,
    uniqueKey: uuidv4()
  },
  {
    name: PROFILE,
    entries: [],
    template: ProfileFields,
    uniqueKey: uuidv4()
  },
  {
    name: EXPERIENCE,
    entries: [],
    template: ExperienceFields,
    uniqueKey: uuidv4()
  },
  {
    name: PROJECTS,
    entries: [],
    template: ProjectFields,
    uniqueKey: uuidv4()
  },
  {
    name: AWARDS,
    entries: [],
    template: AwardFields,
    uniqueKey: uuidv4()
  },
  {
    name: VOLUNTEERING,
    entries: [],
    template: VolunteeringFields,
    uniqueKey: uuidv4()
  },

  {
    name: LANGUAGES,
    entries: [],
    template: LanguageFields,
    uniqueKey: uuidv4()
  }
];
